

#include "MapReduceFramework.h"
#include "MapReduceClient.h"
#include <iostream>
#include <string>
#include <algorithm>
#include <dirent.h>
#include <stdlib.h>
#include <typeinfo>


/** The amount of map/reduce threads in MapReduceFrameWork program */
#define THREAD_AMOUNT 10

/** The minimal amount of arguments to be accepted by the program */
#define MIN_ARGC 2


#define INVALID_ARGC_SIZE_MSG "Usage:<substring to search><folders, separated"\
" by space>"

#define SPACE_STRING " "

#define SEARCH_ERROR_EXIT_CODE 1

#define BAD_CAST_ERROR_MSG "Search program failed: bad casting"



/**
 * A class represents A key of the client in the MapReduceFrameWork
 */
class s_key : public k1Base, public k2Base, public k3Base
{

public:

    /**
     * A constructor for the s_key class. initialized the k string field.
     * @param k A string which represents a key
     */
    s_key(const std::string &k) : k(k) {}

    /**
     * An override of the < operator for k1Base class.
     * @param other: An other k1Base Instance to be compared
     * @return true - if the current instance is < than k1Base instance. false
     * otherwise
     */
    virtual bool operator<(const k1Base &other) const override {
        try
        {
            return comp(dynamic_cast<const s_key &> (other));
        }
        catch (std::bad_cast& e)
        {
            std::cerr << BAD_CAST_ERROR_MSG << std::endl;
            exit(SEARCH_ERROR_EXIT_CODE);
        }
    }

    /**
     * An override of the < operator for k2Base class.
     * @param other: An other k2Base Instance to be compared
     * @return true - if the current instance is < than k2Base instance. false
     * otherwise
     */
    virtual bool operator<(const k2Base &other) const override {
        try
        {
            return comp(dynamic_cast<const s_key &> (other));
        }
        catch (std::bad_cast& e)
        {
            std::cerr << BAD_CAST_ERROR_MSG << std::endl;
            exit(SEARCH_ERROR_EXIT_CODE);
        }
    }

    /**
     * An override of the < operator for k3Base class.
     * @param other: An other k3Base Instance to be compared
     * @return true - if the current instance is < than k3Base instance. false
     * otherwise
     */
    virtual bool operator<(const k3Base &other) const override {
        try
        {
            return comp(dynamic_cast<const s_key &> (other));
        }
        catch (std::bad_cast& e)
        {
            std::cerr << BAD_CAST_ERROR_MSG << std::endl;
            exit(SEARCH_ERROR_EXIT_CODE);
        }
    }

    /**
     * A getter. Returns the value of the k key.
     * @return A string which represents the k key.
     */
    const std::string &getKey() const {
        return k;
    }


private:

    /* A string which represents a key  */
    std::string k;

    /*
     * An helper function for the < operator overrides.
     * Uses the string compare function
     * @param other The other s_key to be compared.
     * @return True, if current key is < than the other key. false otherwise.
     */
    bool comp(const s_key &other) const
    {
        return k.compare(other.k) < 0;
    }
};

/**
 * A class represents A value of the client in the MapReduceFrameWork
 */
class s_value : public v1Base, public v2Base, public v3Base
{

public:

    /**
     * A constructor for the s_value class. initialized the v string field.
     * @param v A string which represents a value
     */
    s_value(const std::string &v) : v(v) {}

    /**
     * A getter. Returns the value of the v value.
     * @return A string which represents the v value.
     */
    const std::string &getValue() const {
        return v;
    }

private:

    /* A string which represents a value  */
    std::string v;

};

/**
 * A class which implements the MapReduce program from user's prepective.
 * Includes Map and Reduce functions.
 */
class MapReduce : public MapReduceBase
{

    /**
     * The user Mapping function. get key and value. If the value string is
     * containd in the key string, The map function send this pair to
     * framework's Emit 2 function
     * @param key  - A k1Base instance. represents a key which will
     * potentially will be contained in value.
     * @param val - A v1Base instance. represents a value in which the key is
     * potentially contained.
     */
    virtual void Map(const k1Base *const key, const v1Base *const val) const
    override
    {

        const s_key* k = dynamic_cast<const s_key *> (key);
        const s_value* v = dynamic_cast<const s_value *> (val);

        if (k == nullptr || v == nullptr)
        {
            std::cerr << BAD_CAST_ERROR_MSG << std::endl;
            exit(SEARCH_ERROR_EXIT_CODE);
        }

        if (v->getValue().find(k->getKey()) != std::string::npos)
        {
            Emit2(const_cast<s_key*> (k) , const_cast<s_value*> (v));
        }
    }

    /**
     * User's Reduce function. The function get a key and a vector of values.
     * and sends the key with each elemtent in the vector, to Emit3 Function.
     * @param key - a k2Base instance.
     * @param vals - A vecvtor of type V2_VEC. each element in the vector is
     * a pointer to a v2Base element.
     */
    virtual void Reduce(const k2Base *const key, const V2_VEC &vals) const
    override
    {

        const s_key* k = dynamic_cast<const s_key *> (key);
        if (k == nullptr)
        {
            std::cerr << BAD_CAST_ERROR_MSG << std::endl;
            exit(SEARCH_ERROR_EXIT_CODE);
        }

        for (auto iter = vals.begin(); iter < vals.end(); ++iter)
        {
            const s_value* v = dynamic_cast<const s_value *> (*iter);
            if (v == nullptr)
            {
                std::cerr << BAD_CAST_ERROR_MSG << std::endl;
                exit(SEARCH_ERROR_EXIT_CODE);
            }

            Emit3(const_cast<s_key*> (k), const_cast<s_value*> (v));
        }
    }
};

/**
 * A comperator. This comperator is used to compare elements of the final
 * program's output. The comperator is used in order to sort the output values
 * @param lhs - The first OUT_ITEM to be compared.
 * @param rhs - The second OUT_ITEM to be compared
 * @return - true, if the value of lhs is smaller than rhs, false otherwise.
 */
bool v3_comparator(const OUT_ITEM& lhs, const OUT_ITEM& rhs)
{
    const s_value* lhs_val = dynamic_cast<const s_value*> (lhs.second);
    const s_value* rhs_val = dynamic_cast<const s_value*> (rhs.second);

    if (lhs_val == nullptr || rhs_val == nullptr)
    {
        std::cerr << BAD_CAST_ERROR_MSG << std::endl;
        exit(SEARCH_ERROR_EXIT_CODE);
    }

    return lhs_val->getValue() < rhs_val->getValue();
}


/**
 * The main function of the Search program. The function parses the given
 * string which will be saved as a k1Base later, and parses the files in the
 * given Directory. It creates an IN_ITEM vector, Runs the RunMapReduceFrameWork
 * Function, The sorts the output, and prints it to the stdout stream.
 * @param argc  - The amount of the arguments the Search program gets
 * @param argv - An array of arguments the vector gets.
 * @return 0, on success, a number which is not 0 on failure.
 */
int main(int argc, char* argv[])
{
    if (argc < MIN_ARGC)
    {
        std::cerr << INVALID_ARGC_SIZE_MSG << std::endl;
        exit(SEARCH_ERROR_EXIT_CODE);
    }

    std::string sub_string(argv[1]);
    s_key sub_string_key(sub_string);

    std::vector<s_value> files;
    for (int i = 2; i < argc; ++i)
    {
        DIR* directory;
        struct dirent* struct_dirent;

        directory = opendir(argv[i]);
        if(directory == NULL)
        {
            continue;
        }


        while((struct_dirent = readdir(directory)) != NULL)
        {
            files.push_back(std::string(struct_dirent->d_name));
        }


        if(closedir(directory) == -1)
        {
            continue;
        }

    }

    // creating framework's input vector.
    IN_ITEMS_VEC itemsVec;
    for (auto iter1 = files.begin(); iter1 < files.end(); ++iter1)
    {
        IN_ITEM curr_item(&sub_string_key, &(*iter1));
        itemsVec.push_back(curr_item);
    }


    MapReduce mr;
    OUT_ITEMS_VEC res =
            RunMapReduceFramework(mr, itemsVec, THREAD_AMOUNT, false);

    std::sort(res.begin(), res.end(), v3_comparator);

    // printing the final output to std out
    for (auto iter2 = res.begin(); iter2 < res.end(); )
    {
        const OUT_ITEM & oi = *iter2;
        const s_value* output_val = dynamic_cast<const s_value *> (oi.second);
        if (output_val == nullptr)
        {
            std::cerr << BAD_CAST_ERROR_MSG << std::endl;
            exit(SEARCH_ERROR_EXIT_CODE);
        }

        std::cout << output_val->getValue();
        if (++iter2 < res.end())
        {
            std::cout << SPACE_STRING;
        }
    }

    return 0;
}
