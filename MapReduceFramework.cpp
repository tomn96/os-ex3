

#include "MapReduceFramework.h"
#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <fstream>
#include <iomanip>
#include <sys/time.h>
#include <string>

// /////////////////////////// defines //////////////////////////////////////

/** The amount of elements each map and reduce thread grabs in each turn */
#define CHUNK_SIZE 10

#define ERROR_EXIT_CODE 1

#define SEC_TO_NANOSEC 1000000000

#define MICROSEC_TO_NANOSEC 1000

#define FRAMEWORK_FAIL_MSG "MapReduceFramework Failure: "

#define FAIL_STR " failed"

#define MUTEX_LOCK_STR "pthread_mutex_lock"

#define UNLOCK_MUTEX_STR "pthread_mutex_unlock"

#define MUTEX_INIT_STR "pthread_mutex_init"

#define MUTEX_DSTRY_STR "pthread_mutex_destroy"

#define NANO_SEC_STR " ns"

#define SEM_WAIT_STR "sem_wait"

#define SEM_POST_STR "sem_post"

#define SEM_INIT_STR "sem_init"

#define SEM_DSTRY_STR "sem_destroy"

#define THREAD_CREATE_STR "pthread_create"

#define LOG_SHUFFLE_CREATE_STR "Thread Shuffle created "

#define LOG_MAP_THREAD_CREATE "Thread ExecMap created "

#define LOG_REDUCE_THREAD_CREATE "Thread ExecReduce created "

#define LOG_SHUFFLE_THRD_TERMINATE "Thread Shuffle terminated "

#define LOG_MAP_THRD_TERMINATE "Thread ExecMap terminated "

#define LOG_REDUCE_THRD_TERMINATE "Thread ExecReduce terminated "

#define DATE_AND_TIME_FORMAT "[%d.%m.%Y %T]"

#define THREAD_JOIN_STR "pthread_join"

#define LOG_FILE_NAME ".MapReduceFramework.log"

#define LOG_PROGRAM_START_STR "RunMapReduceFramework started with "

#define THREADS_STR " threads"

#define LOG_PROGRAM_FINISH_STR "RunMapReduceFramework finished"

#define MAP_AND_SHUFFLE_TIME_TOOK "Map and Shuffle took "

#define REDUCE_TIME_TOOK "Reduce took "

#define LOG_OPEN_STR "log.open"

#define LOG_CLOSE_STR "log.close"


/**
 * struct used to compare two k2Base* .
 */
struct k2Base_comp
{
    bool operator() (const k2Base* lhs, const k2Base* rhs) const
    {return *lhs < *rhs;}
};


// //////////////////// typedef ///////////////////////////////

typedef std::pair<k2Base*, v2Base*> MID_ITEM;
typedef std::vector<MID_ITEM> MID_ITEMS_VEC;

typedef std::pair<k2Base*, V2_VEC> SHUFFLE_PAIR;
typedef std::map<k2Base*, V2_VEC, k2Base_comp> SHUFFLE_MAP;


// //////////////////// Function declarations ///////////////////////////////

void* execMap(void*);
void* shuffle(void*);
void* execReduce(void*);



// ///////////////////////////// Threads Structs ////////////////////////////

/**
 * struct representing the Map Thread.
 * It includes: the threads's pthread_t, the container in which the map stores
 * its outputs (using Emit2), and the mutex for this container.
 */
struct MapThread
{
    pthread_t pthread;
    MID_ITEMS_VEC container;
    pthread_mutex_t container_mutex;
};


/**
 * struct representing the Shuffle Thread.
 * It includes: the threads's pthread_t, and the container in which the
 * shuffle stores its outputs.
 */
struct ShuffleThread
{
    pthread_t pthread;
    SHUFFLE_MAP container;
};

/**
 * struct representing the Reduce Thread.
 * It includes: the threads's pthread_t, and the container in which the reduce
 * stores its outputs (using Emit3).
 */
struct ReduceThread
{
    pthread_t pthread;
    OUT_ITEMS_VEC container;
};


// ////////////////////////// Global Variables ///////////////////////////////

/** a pointer to the MapReduceBase implemented and provided by the user */
MapReduceBase* g_mapReduce = nullptr;

/** a pointer to the input vector provided by the user */
IN_ITEMS_VEC* g_itemsVec = nullptr;

/** a boolean represents if the user asked to perform auto delete */
bool g_autoDeleteV2K2 = false;


/** a vector containing all the map threads of the framework's run */
std::vector<MapThread> map_threads;

/** the Shuffle thread of the framework's run */
ShuffleThread shuffle_thread;

/** a vector containing all the reduce threads of the framework's run */
std::vector<ReduceThread> reduce_threads;


/** an index which is used in order to know who are the currently treated
 * inputs of the input vector ("g_itemsVec") */
size_t input_index = 0;

/** a mutex used to lock any changes in "input_index" */
pthread_mutex_t input_index_mutex;


/** an iterator which is used in order to run over the Shuffle output vector
 * in the reduce threads */
SHUFFLE_MAP::iterator reduce_iter;

/** a mutex used to lock any changes in "reduce_iter" */
pthread_mutex_t reduce_iter_mutex;


/** a boolean represents whether all of the map threads finished */
bool all_map_threads_finished = false;

/** a semaphore used to handle the simultaneous run of the map threads with
 * the shuffle thread */
sem_t semaphore;


/** an fstream which is the log file to write to */
std::fstream log;

/** a mutex used to lock any actions on the "log" fstream */
pthread_mutex_t log_mutex;




// //////////////////////// write to LOG functions ///////////////////////////

/**
 * writes a string to the log
 * @param to_log The string to write
 */
void log_print(std::string to_log)
{
    if (pthread_mutex_lock(&log_mutex))
    {
        std::cerr <<FRAMEWORK_FAIL_MSG <<MUTEX_LOCK_STR<< FAIL_STR<< std::endl;
        exit(ERROR_EXIT_CODE);
    }

    log << to_log << std::endl;

    if (pthread_mutex_unlock(&log_mutex))
    {
        std::cerr <<FRAMEWORK_FAIL_MSG<<UNLOCK_MUTEX_STR<<FAIL_STR<< std::endl;
        exit(ERROR_EXIT_CODE);
    }
}


/**
 * write a string to the log with the current time
 * @param to_log The string to write
 */
void log_current_time(std::string to_log)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);

    if (pthread_mutex_lock(&log_mutex))
    {
        std::cerr <<FRAMEWORK_FAIL_MSG <<MUTEX_LOCK_STR<< FAIL_STR<< std::endl;
        exit(ERROR_EXIT_CODE);
    }

    log << to_log << std::put_time(localtime(&tv.tv_sec),
                                   DATE_AND_TIME_FORMAT) << std::endl;

    if (pthread_mutex_unlock(&log_mutex))
    {
        std::cerr <<FRAMEWORK_FAIL_MSG<<UNLOCK_MUTEX_STR<<FAIL_STR<< std::endl;
        exit(ERROR_EXIT_CODE);
    }
}


/**
 * writes string to log with the given elasped time
 * @param to_log The string to write
 * @param start The start time (for calculation of elasped time)
 * @param end The end time (for calculation of elasped time)
 */
void log_elasped_time(std::string to_log, struct timeval& start,
                      struct timeval& end)
{
    long s = (start.tv_sec * SEC_TO_NANOSEC)
             + (start.tv_usec * MICROSEC_TO_NANOSEC);
    long e = (end.tv_sec * SEC_TO_NANOSEC)
             + (end.tv_usec * MICROSEC_TO_NANOSEC);
    long passed = e - s;

    if (pthread_mutex_lock(&log_mutex))
    {
        std::cerr <<FRAMEWORK_FAIL_MSG <<MUTEX_LOCK_STR<< FAIL_STR<< std::endl;
        exit(ERROR_EXIT_CODE);
    }

    log << to_log << passed << NANO_SEC_STR << std::endl;

    if (pthread_mutex_unlock(&log_mutex))
    {
        std::cerr <<FRAMEWORK_FAIL_MSG<<UNLOCK_MUTEX_STR<<FAIL_STR<< std::endl;
        exit(ERROR_EXIT_CODE);
    }
}


// /////////////////////// Thread's Functions ////////////////////////////////

/**
 * the ExecMap thread function
 * @param arg An argument for the thread's function
 * @return NULL
 */
void* execMap(void* arg)
{
    while (input_index < g_itemsVec->size())
    {
        if (pthread_mutex_lock(&input_index_mutex))
        {
            std::cerr<<FRAMEWORK_FAIL_MSG<<MUTEX_LOCK_STR<<FAIL_STR<<std::endl;
            exit(ERROR_EXIT_CODE);
        }

        size_t start = input_index;
        input_index += CHUNK_SIZE;

        if (pthread_mutex_unlock(&input_index_mutex))
        {
            std::cerr <<FRAMEWORK_FAIL_MSG<<UNLOCK_MUTEX_STR<<
                     FAIL_STR<< std::endl;
            exit(ERROR_EXIT_CODE);
        }


        for (size_t i = start;
             i < start + CHUNK_SIZE && i < g_itemsVec->size(); ++i)
        {
            g_mapReduce->Map(g_itemsVec->at(i).first,
                             g_itemsVec->at(i).second);
        }
    }

    return NULL;
}


/**
 * moves items from the map containers to the shuffle container.
 * @param move_single_item If true the function will move only one item.
 */
void move_from_map_to_shuffle(bool move_single_item)
{
    for (auto iter1 = map_threads.begin(); iter1 < map_threads.end(); ++iter1)
    {
        while (!(*iter1).container.empty())
        {
            if (pthread_mutex_lock(&(*iter1).container_mutex))
            {
                std::cerr<<FRAMEWORK_FAIL_MSG<<MUTEX_LOCK_STR
                         <<FAIL_STR<<std::endl;
                exit(ERROR_EXIT_CODE);
            }

            MID_ITEM current_pair = (*iter1).container.back();
            (*iter1).container.pop_back();

            if (pthread_mutex_unlock(&(*iter1).container_mutex))
            {
                std::cerr <<FRAMEWORK_FAIL_MSG<<UNLOCK_MUTEX_STR
                          <<FAIL_STR<< std::endl;
                exit(ERROR_EXIT_CODE);
            }


            SHUFFLE_MAP::iterator shuffle_key =
                    shuffle_thread.container.find(current_pair.first);
            if (shuffle_key == shuffle_thread.container.end())
            {
                shuffle_thread.container.insert(
                        SHUFFLE_PAIR(current_pair.first,
                                     {current_pair.second}));
            }
            else
            {
                (*shuffle_key).second.push_back(current_pair.second);
                if (g_autoDeleteV2K2)
                {
                    delete current_pair.first;
                }
            }


            if (move_single_item) { return; }
        }
    }
}



/**
 * the Shuffle thread function
 * @param arg An argument for the thread's function
 * @return NULL
 */
void* shuffle(void* arg)
{
    while (!all_map_threads_finished)
    {
        if (sem_wait(&semaphore))
        {
            std::cerr << FRAMEWORK_FAIL_MSG<<SEM_WAIT_STR
                      <<FAIL_STR<< std::endl;
            exit(ERROR_EXIT_CODE); }

        move_from_map_to_shuffle(true);
    }

    move_from_map_to_shuffle(false);

    return NULL;
}



/**
 * the ExecReduce thread function
 * @param arg An argument for the thread's function
 * @return NULL
 */
void* execReduce(void* arg)
{
    while (reduce_iter != shuffle_thread.container.end())
    {
        if (pthread_mutex_lock(&reduce_iter_mutex))
        {
            std::cerr<<FRAMEWORK_FAIL_MSG<<MUTEX_LOCK_STR<<FAIL_STR<<std::endl;
            exit(ERROR_EXIT_CODE);
        }

        SHUFFLE_MAP::iterator start = reduce_iter;

        for (int j = 0;
             j < CHUNK_SIZE && reduce_iter != shuffle_thread.container.end();
             ++j) { ++reduce_iter; }

        SHUFFLE_MAP::iterator end = reduce_iter;

        if (pthread_mutex_unlock(&reduce_iter_mutex))
        {
            std::cerr<<FRAMEWORK_FAIL_MSG<<UNLOCK_MUTEX_STR
                     <<FAIL_STR<<std::endl;
            exit(ERROR_EXIT_CODE);
        }


        for (; start != end; ++start)
        {
            g_mapReduce->Reduce((*start).first, (*start).second);
        }
    }

    return NULL;
}



/**
 * function used by the user in their map implementation in order to move an
 * item to the next stage.
 * @param key Pointer to the key of the item
 * @param val Pointer to the value of the item
 */
void Emit2 (k2Base* key, v2Base* val)
{
    pthread_t self = pthread_self();

    for (auto iter = map_threads.begin(); iter < map_threads.end(); ++iter)
    {
        if ((*iter).pthread == self)
        {
            if (pthread_mutex_lock(&(*iter).container_mutex))
            {
                std::cerr<<FRAMEWORK_FAIL_MSG<<MUTEX_LOCK_STR
                         <<FAIL_STR<<std::endl;
                exit(ERROR_EXIT_CODE);
            }

            MID_ITEM curr_item = {key, val};
            (*iter).container.push_back(curr_item);
            if (sem_post(&semaphore))
            {
                std::cerr << FRAMEWORK_FAIL_MSG << SEM_POST_STR
                          <<FAIL_STR<< std::endl;
                exit(ERROR_EXIT_CODE);
            }

            if (pthread_mutex_unlock(&(*iter).container_mutex))
            {
                std::cerr<<FRAMEWORK_FAIL_MSG<<UNLOCK_MUTEX_STR
                         <<FAIL_STR<<std::endl;
                exit(ERROR_EXIT_CODE);
            }

            break;
        }
    }
}

/**
 * function used by the user in their Reduce implementation in order to move
 * an item to the final output.
 * @param key Pointer to the key of the item
 * @param val Pointer to the value of the item
 */
void Emit3 (k3Base* key, v3Base* val)
{
    pthread_t self = pthread_self();

    for (auto iter = reduce_threads.begin();
         iter < reduce_threads.end(); ++iter)
    {
        if ((*iter).pthread == self)
        {
            OUT_ITEM curr_item = {key, val};
            (*iter).container.push_back(curr_item);

            break;
        }
    }
}


/**
 * creates the final output to the user from the reduce threds treated data
 * @param result Reference to the final result vector in which this function
 * writes the result data.
 */
void create_final_output(OUT_ITEMS_VEC& result)
{
    for (auto reduce_iter = reduce_threads.begin();
         reduce_iter < reduce_threads.end(); ++reduce_iter)
    {
        for (auto pair_iter = (*reduce_iter).container.begin();
             pair_iter < (*reduce_iter).container.end(); ++pair_iter)
        {
            result.push_back((*pair_iter));
        }
    }

    std::sort(result.begin(), result.end(),
              [](const OUT_ITEM& lhs, const OUT_ITEM& rhs)
              { return (*lhs.first) < (*rhs.first); });
}

// /////////// helping functions for the map and shuffle run //////////////////


/**
 * initiates all the required tools (semaphore and mutex) for the map and the
 * shuffle threads.
 */
void init_map_and_shuffle_tools()
{
    if (sem_init(&semaphore, 0, 0))
    {
        std::cerr << FRAMEWORK_FAIL_MSG << SEM_INIT_STR <<FAIL_STR<< std::endl;
        exit(ERROR_EXIT_CODE);
    }

    if (pthread_mutex_init(&input_index_mutex, NULL))
    {
        std::cerr <<FRAMEWORK_FAIL_MSG<<MUTEX_INIT_STR<<FAIL_STR<< std::endl;
        exit(ERROR_EXIT_CODE);
    }
}


/**
 * destroys all the required tools (semaphore and mutex) for the map and the
 * shuffle threads.
 */
void destroy_map_and_shuffle_tools()
{
    if (pthread_mutex_destroy(&input_index_mutex))
    {
        std::cerr << FRAMEWORK_FAIL_MSG<<MUTEX_DSTRY_STR<<FAIL_STR <<std::endl;
        exit(ERROR_EXIT_CODE);
    }

    if (sem_destroy(&semaphore))
    {
        std::cerr << FRAMEWORK_FAIL_MSG<< SEM_DSTRY_STR <<FAIL_STR<< std::endl;
        exit(ERROR_EXIT_CODE);
    }
}

/**
 * create the vector of the map threads
 * @param multiThreadLevel amount of map threads to create
 */
void create_map_threads_vector(int multiThreadLevel)
{
    for (int i = 0; i < multiThreadLevel; ++i)
    {
        map_threads.push_back(MapThread());
    }
}

/**
 * create the mutex for each of the map threads
 */
void create_mapExecs_mutex()
{
    for (auto iter = map_threads.begin(); iter < map_threads.end(); ++iter)
    {
        if (pthread_mutex_init(&(*iter).container_mutex, NULL))
        {
            std::cerr<<FRAMEWORK_FAIL_MSG<<MUTEX_INIT_STR<<FAIL_STR<<std::endl;
            exit(ERROR_EXIT_CODE);
        }
    }
}

/**
 * destroy the mutex for each of the map threads
 */
void destroy_mapExecs_mutex()
{
    for (auto iter = map_threads.begin(); iter < map_threads.end(); ++iter)
    {
        if (pthread_mutex_destroy(&(*iter).container_mutex))
        {
            std::cerr<<FRAMEWORK_FAIL_MSG<<MUTEX_DSTRY_STR<<FAIL_STR<<std::endl;
            exit(ERROR_EXIT_CODE);
        }
    }
}


/**
 * create the pthread for each of the map threads
 */
void create_mapExecs()
{
    for (auto iter = map_threads.begin(); iter < map_threads.end(); ++iter)
    {
        if (pthread_create(&(*iter).pthread, NULL, execMap, NULL))
        {
            std::cerr << FRAMEWORK_FAIL_MSG << THREAD_CREATE_STR
                      << FAIL_STR << std::endl;
            exit(ERROR_EXIT_CODE);
        }

        log_current_time(LOG_MAP_THREAD_CREATE);
    }
}

/**
 * create the mutex and the pthread for each of the map threads
 */
void create_mapExecs_and_mutex()
{
    create_mapExecs_mutex();
    create_mapExecs();
}


/**
 * create the pthread for the shuffle thread
 */
void create_shuffle()
{
    if (pthread_create(&shuffle_thread.pthread, NULL, shuffle, NULL))
    {
        std::cerr <<FRAMEWORK_FAIL_MSG<< THREAD_CREATE_STR
                  << FAIL_STR << std::endl;
        exit(ERROR_EXIT_CODE);
    }

    log_current_time(LOG_SHUFFLE_CREATE_STR);
}

/**
 * create (start) the map and the shuffle threads
 */
void start_map_and_shuffle()
{
    create_mapExecs_and_mutex();
    create_shuffle();
}



/**
 * joins (waits for) all of the map threads
 */
void join_map_threads()
{
    for (auto iter = map_threads.begin(); iter < map_threads.end(); ++iter)
    {
        if(pthread_join((*iter).pthread, NULL))
        {
            std::cerr << FRAMEWORK_FAIL_MSG << THREAD_JOIN_STR
                      <<FAIL_STR << std::endl;
            exit(ERROR_EXIT_CODE);
        }

        log_current_time(LOG_MAP_THRD_TERMINATE);
    }
}


/**
 * joins (waits for) the shuffle thread
 */
void join_shuffle_thread()
{
    if(pthread_join((shuffle_thread.pthread), NULL))
    {
        std::cerr << FRAMEWORK_FAIL_MSG << THREAD_JOIN_STR
                  <<FAIL_STR << std::endl;
        exit(ERROR_EXIT_CODE);
    };

    log_current_time(LOG_SHUFFLE_THRD_TERMINATE);
}


// /////////// helping functions for the reduce run //////////////////

/**
 * initiates all the required tools (mutex) for the reduce threads.
 */
void init_reduce_tools()
{
    if (pthread_mutex_init(&reduce_iter_mutex, NULL))
    {
        std::cerr << FRAMEWORK_FAIL_MSG<< MUTEX_INIT_STR<<FAIL_STR<< std::endl;
        exit(ERROR_EXIT_CODE);
    }
}

/**
 * destroys all the required tools (mutex) for the reduce threads.
 */
void destroy_reduce_tools()
{
    if(pthread_mutex_destroy(&reduce_iter_mutex))
    {
        std::cerr <<FRAMEWORK_FAIL_MSG <<MUTEX_DSTRY_STR<<FAIL_STR<<std::endl;
        exit(ERROR_EXIT_CODE);
    }
}

/**
 * create the vector of the reduce threads
 * @param multiThreadLevel amount of reduce threads to create
 */
void create_reduce_threads_vector(int multiThreadLevel)
{
    for (int i = 0; i < multiThreadLevel; ++i)
    {
        reduce_threads.push_back(ReduceThread());
    }
}

/**
 * create (start) the reduce threads
 */
void start_reduceExecs()
{
    for (auto iter=reduce_threads.begin(); iter<reduce_threads.end(); ++iter)
    {
        if (pthread_create(&(*iter).pthread, NULL, execReduce, NULL))
        {
            std::cerr << FRAMEWORK_FAIL_MSG << THREAD_CREATE_STR
                      << FAIL_STR << std::endl;
            exit(ERROR_EXIT_CODE);
        }
        log_current_time(LOG_REDUCE_THREAD_CREATE);
    }
}

/**
 * joins (waits for) the reduce threads
 */
void join_reduce_threads()
{
    for (auto iter2 = reduce_threads.begin();
         iter2 < reduce_threads.end();
         ++iter2)
    {
        if(pthread_join((*iter2).pthread, NULL))
        {
            std::cerr << FRAMEWORK_FAIL_MSG << THREAD_JOIN_STR
                      <<FAIL_STR << std::endl;
            exit(ERROR_EXIT_CODE);
        }
        log_current_time(LOG_REDUCE_THRD_TERMINATE);
    }
}


// ///// delete the rest of the V2K2 for auto delete functionality ///////////

/**
 * This function deletes the rest of the k2Base and v2Base in the program in
 * order to fulfill the auto delete functionality of the framework.
 */
void delete_V2K2_vec()
{
        for (auto iter1 = shuffle_thread.container.begin();
             iter1 != shuffle_thread.container.end(); ++iter1)
        {
            for (auto iter2 = (*iter1).second.begin();
                 iter2 < (*iter1).second.end(); ++iter2)
            {
                delete (*iter2);
            }
            delete (*iter1).first;
        }
}



// /////////////////////// start and end the log /////////////////////////////

/**
 * starts the log. i.e. opens the log and create an initial log commit
 * @param multiThreadLevel The amount of Threads requested by the user
 */
void start_log(int multiThreadLevel)
{
    if (pthread_mutex_init(&log_mutex, NULL))
    {
        std::cerr << FRAMEWORK_FAIL_MSG <<MUTEX_INIT_STR<<FAIL_STR<< std::endl;
        exit(ERROR_EXIT_CODE);
    }

    log.open(LOG_FILE_NAME, std::fstream::out | std::fstream::app);
    if (log.fail())
    {
        std::cerr <<FRAMEWORK_FAIL_MSG<<LOG_OPEN_STR<<FAIL_STR<<std::endl;
        exit(ERROR_EXIT_CODE);
    }

    std::string message = std::string(LOG_PROGRAM_START_STR)
                          + std::to_string(multiThreadLevel)
                          + std::string(THREADS_STR);
    log_print(message);
}



/**
 * ends the log. i.e. closes the log and create a final log commit
 * @param multiThreadLevel The amount of Threads requested by the user
 */
void end_log()
{
    log_print(std::string(LOG_PROGRAM_FINISH_STR));

    log.close();
    if(log.fail())
    {
        std::cerr <<FRAMEWORK_FAIL_MSG<<LOG_CLOSE_STR<<FAIL_STR<<std::endl;
        exit(ERROR_EXIT_CODE);
    }
    if (pthread_mutex_destroy(&log_mutex))
    {
        std::cerr <<FRAMEWORK_FAIL_MSG<<MUTEX_DSTRY_STR <<FAIL_STR<<std::endl;
        exit(ERROR_EXIT_CODE);
    }
}



/**
 * The main function that the user calls in order to use (and run) the
 * framework. It runs the map-reduce logic and returns the final result.
 * @param mapReduce A MapReduceBase instance implemented by the user,
 * in which there are their map and reduce functions.
 * @param itemsVec The vector of the input items on which the framework
 * operates and runs the map-reduce logic.
 * @param multiThreadLevel The amount of Threads requested by the user. i.e.
 * there will be multiThreadLevel map threads, 1 shuffle thread, and
 * multiThreadLevel reduce threads. Therefor the total amount of threads
 * is 2*multiThreadLevel + 1 .
 * @param autoDeleteV2K2 A boolean that tells whether tha framework should
 * execute its auto delete functionality
 * @return The final result vector after the framework ran the map-reducr
 * logic on the input "itemsVec".
 */
OUT_ITEMS_VEC RunMapReduceFramework(MapReduceBase& mapReduce,
                                    IN_ITEMS_VEC& itemsVec,
                                    int multiThreadLevel, bool autoDeleteV2K2)
{

    // /////////////// initial operations //////////////

    start_log(multiThreadLevel);

    g_mapReduce = &mapReduce;
    g_itemsVec = &itemsVec;
    g_autoDeleteV2K2 = autoDeleteV2K2;

    map_threads.clear();
    shuffle_thread.container.clear();
    reduce_threads.clear();

    input_index = 0;
    all_map_threads_finished = false;

    struct timeval map_and_shuffle_start;
    struct timeval map_and_shuffle_end;
    struct timeval reduce_start;
    struct timeval reduce_end;


    // ///////// creation and operation of map and shuffle threads ///////////

    init_map_and_shuffle_tools();
    create_map_threads_vector(multiThreadLevel);

    gettimeofday(&map_and_shuffle_start, NULL);

    start_map_and_shuffle();
    join_map_threads();
    all_map_threads_finished = true;

    sem_post(&semaphore);
    join_shuffle_thread();

    gettimeofday(&map_and_shuffle_end, NULL);
    log_elasped_time(std::string(MAP_AND_SHUFFLE_TIME_TOOK),
                     map_and_shuffle_start, map_and_shuffle_end);

    reduce_iter = shuffle_thread.container.begin();

    destroy_mapExecs_mutex();
    destroy_map_and_shuffle_tools();



    // /////////////// creation and operation of reduce threads //////////////


    init_reduce_tools();
    create_reduce_threads_vector(multiThreadLevel);

    gettimeofday(&reduce_start, NULL);

    start_reduceExecs();
    join_reduce_threads();

    gettimeofday(&reduce_end, NULL);
    log_elasped_time(std::string(REDUCE_TIME_TOOK), reduce_start, reduce_end);

    destroy_reduce_tools();


    // /////////////// final operations //////////////

    if (autoDeleteV2K2) { delete_V2K2_vec(); }


    OUT_ITEMS_VEC final_output;
    create_final_output(final_output);

    end_log();

    return final_output;
}
