CXX= g++
CXXFLAGS= -c -Wall -std=c++11 -pthread -DNDEBUG
CXXFLAGS2= -Wall -std=c++11 -pthread -DNDEBUG
CODEFILES= ex3.tar Search.cpp MapReduceFramework.cpp Makefile README
LIBOBJECTS= MapReduceFramework.o


# Default
default: MapReduceFramework Search


# Executables
MapReduceFramework: MapReduceFramework.o
	ar rcs MapReduceFramework.a $(LIBOBJECTS)

Search: MapReduceFramework Search.o
	$(CXX) $(CXXFLAGS2) Search.o -L. MapReduceFramework.a -lpthread -o Search


# Object Files
Search.o: MapReduceFramework.h MapReduceClient.h Search.cpp
	$(CXX) $(CXXFLAGS) Search.cpp -o Search.o

MapReduceFramework.o: MapReduceFramework.h MapReduceFramework.cpp
	$(CXX) $(CXXFLAGS) MapReduceFramework.cpp -o MapReduceFramework.o


# tar
tar:
	tar -cvf $(CODEFILES)


# Other Targets
clean:
	-rm -f *.o *.a *.tar Search .MapReduceFramework.log

